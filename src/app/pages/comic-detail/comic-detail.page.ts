import { Component, OnInit } from '@angular/core';
import { ComicsService } from 'src/app/services/comics/comics.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-comic-detail',
  templateUrl: './comic-detail.page.html',
  styleUrls: ['./comic-detail.page.scss'],
})
export class ComicDetailPage implements OnInit {

  id: number;
  comic: any;

  constructor(
    private comicsService: ComicsService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.id = + this.activatedRoute.snapshot.paramMap.get('id');
    this.getComic();
  }

  getComic() {
    this.comicsService.getComic(this.id)
      .then((data: any) => {
        this.comic = data.data.results[0];
        console.log('Data comic: ', this.comic);
        this.addPathToComic();
        this.addImgToComic();
      })
      .catch((error: any) => {
        console.error(error);
      });
  }

  addPathToComic() {
    this.comic.characters.items.forEach((character, index) => {
      this.comic.characters.items[index].id = + character.resourceURI.split('characters/')[1];
    });
  }

  addImgToComic() {
    this.comic.thumbnail.img = this.comic.thumbnail.path + '.' + this.comic.thumbnail.extension;
  }

  detailCharacter(id: number) {
    this.router.navigateByUrl('character-detail/' + id);
  }

}
