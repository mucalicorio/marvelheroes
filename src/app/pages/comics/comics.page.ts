import { Component, OnInit } from '@angular/core';
import { ComicsService } from 'src/app/services/comics/comics.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-comics',
  templateUrl: './comics.page.html',
  styleUrls: ['./comics.page.scss'],
})
export class ComicsPage implements OnInit {

  comics: any = [];
  limit = 20;
  offset = 0;

  constructor(
    private comicsService: ComicsService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.getAll();
  }

  getAll() {
    this.comicsService.getComics(this.limit, this.offset)
      .then((data: any) => {
        this.comics.push(...data.data.results);
        console.log(this.comics);
        this.addImgToComics();
      })
      .catch((error) => {
        console.error(error);
      });

    this.offset += 20;
  }

  addImgToComics() {
    this.comics.forEach((comic, index) => {
      this.comics[index].thumbnail.img = comic.thumbnail.path + '.' + comic.thumbnail.extension;
    });
  }

  detailComic(id: number) {
    this.router.navigateByUrl('comic-detail/' + id);
  }

  async doRefresh(event) {
    this.offset = 0;
    await this.getAll();

    event.target.complete();
  }

}
