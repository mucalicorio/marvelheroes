import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CreatorsService } from 'src/app/services/creators/creators.service';

@Component({
  selector: 'app-creator-detail',
  templateUrl: './creator-detail.page.html',
  styleUrls: ['./creator-detail.page.scss'],
})
export class CreatorDetailPage implements OnInit {

  id: number;
  creator: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private creatorsService: CreatorsService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.id = + this.activatedRoute.snapshot.paramMap.get('id');
    this.getCreator();
  }

  getCreator() {
    this.creatorsService.getCreator(this.id)
      .then((data: any) => {
        this.creator = data.data.results[0];
        console.log('Data creator: ', this.creator);
        this.addPathToCreator();
        this.addImgToCreator();
      })
      .catch((error: any) => {
        console.error(error);
      });
  }

  addPathToCreator() {
    this.creator.comics.items.forEach((comic, index) => {
      this.creator.comics.items[index].id = + comic.resourceURI.split('comics/')[1];
    });
  }

  addImgToCreator() {
    this.creator.thumbnail.img = this.creator.thumbnail.path + '.' + this.creator.thumbnail.extension;
  }

  detailComic(id: number) {
    this.router.navigateByUrl('comic-detail/' + id);
  }

}
