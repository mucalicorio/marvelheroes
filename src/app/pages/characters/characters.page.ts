import { Component, OnInit } from '@angular/core';
import { CharactersService } from 'src/app/services/characters/characters.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.page.html',
  styleUrls: ['./characters.page.scss'],
})
export class CharactersPage implements OnInit {

  characters: any = [];
  limit = 20;
  offset = 0;

  constructor(private charactersService: CharactersService, private router: Router) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.getAll();
  }

  getAll() {
    this.charactersService.getCharacters(this.limit, this.offset)
      .then((data: any) => {
        this.characters.push(...data.data.results);
        console.log(this.characters);
        this.addImgToCharacters();
      })
      .catch((error) => {
        console.error(error);
      });

    this.offset += 20;
  }

  addImgToCharacters() {
    this.characters.forEach((character, index) => {
      this.characters[index].thumbnail.img = character.thumbnail.path + '.' + character.thumbnail.extension;
    });
  }

  detailCharacter(id: number) {
    this.router.navigateByUrl('character-detail/' + id);
  }

  async doRefresh(event) {
    this.offset = 0;
    await this.getAll();

    event.target.complete();
  }

}
