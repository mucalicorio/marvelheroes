import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CharactersService } from 'src/app/services/characters/characters.service';

@Component({
  selector: 'app-character-detail',
  templateUrl: './character-detail.page.html',
  styleUrls: ['./character-detail.page.scss'],
})
export class CharacterDetailPage implements OnInit {

  id: number;
  character: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private charactersService: CharactersService
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.id = + this.activatedRoute.snapshot.paramMap.get('id');
    this.getCharacter();
  }

  getCharacter() {
    this.charactersService.getCharacter(this.id)
      .then((data: any) => {
        this.character = data.data.results[0];
        console.log('Data character: ', this.character);
        this.addPathToComics();
        this.addImgToCharacters();
      })
      .catch((error: any) => {
        console.error(error);
      });
  }

  addPathToComics() {
    this.character.comics.items.forEach((comic, index) => {
      this.character.comics.items[index].id = + comic.resourceURI.split('comics/')[1];
    });
  }

  addImgToCharacters() {
    this.character.thumbnail.img = this.character.thumbnail.path + '.' + this.character.thumbnail.extension;
  }

  detailComic(id) {
    this.router.navigateByUrl('comic-detail/' + id);
  }

}
