import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatorsPage } from './creators.page';

describe('CreatorsPage', () => {
  let component: CreatorsPage;
  let fixture: ComponentFixture<CreatorsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatorsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatorsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
