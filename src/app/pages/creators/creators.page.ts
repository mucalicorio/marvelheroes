import { Component, OnInit } from '@angular/core';
import { CreatorsService } from 'src/app/services/creators/creators.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-creators',
  templateUrl: './creators.page.html',
  styleUrls: ['./creators.page.scss'],
})
export class CreatorsPage implements OnInit {

  creators: any = [];
  limit = 20;
  offset = 0;

  constructor(
    public creatorsService: CreatorsService,
    public router: Router
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.getAll();
  }

  getAll() {
    this.creatorsService.getCreators(this.limit, this.offset)
      .then((data: any) => {
        this.creators.push(...data.data.results);
        console.log(this.creators);
        this.addImgToCreators();
      })
      .catch((error) => {
        console.error(error);
      });

    this.offset += 20;
  }

  addImgToCreators() {
    this.creators.forEach((creator, index) => {
      this.creators[index].thumbnail.img = creator.thumbnail.path + '.' + creator.thumbnail.extension;
    });
  }

  detailCreator(id: number) {
    this.router.navigateByUrl('creator-detail/' + id);
  }

  async doRefresh(event) {
    this.offset = 0;
    await this.getAll();

    event.target.complete();
  }

}
