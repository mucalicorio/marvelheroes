import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  // { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'home', loadChildren: './pages/home/home.module#HomePageModule' },
  { path: 'characters', loadChildren: './pages/characters/characters.module#CharactersPageModule' },
  { path: 'comics', loadChildren: './pages/comics/comics.module#ComicsPageModule' },
  { path: 'creators', loadChildren: './pages/creators/creators.module#CreatorsPageModule' },
  { path: 'character-detail/:id', loadChildren: './pages/character-detail/character-detail.module#CharacterDetailPageModule' },
  { path: 'comic-detail/:id', loadChildren: './pages/comic-detail/comic-detail.module#ComicDetailPageModule' },
  { path: 'creator-detail/:id', loadChildren: './pages/creator-detail/creator-detail.module#CreatorDetailPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
