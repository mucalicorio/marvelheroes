import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';

@Injectable({
  providedIn: 'root'
})
export class CreatorsService {

  constructor(
    private api: ApiService
  ) { }

  getCreators(limit: number = 10, offset: number) {
    return new Promise((resolve, reject) => {
      this.api.get('v1/public/creators', limit, offset)
        .subscribe((data) => {
          resolve(data);
        }, (error) => {
          reject(error);
        });
    });
  }

  getCreator(id: number) {
    return new Promise((resolve, reject) => {
      this.api.get('v1/public/creators/' + id)
        .subscribe((data) => {
          resolve(data);
        }, (error) => {
          reject(error);
        });
    });
  }
}
