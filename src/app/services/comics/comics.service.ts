import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';

@Injectable({
  providedIn: 'root'
})
export class ComicsService {

  constructor(
    private api: ApiService
  ) { }

  getComics(limit: number = 10, offset: number) {
    return new Promise((resolve, reject) => {
      this.api.get('v1/public/comics', limit, offset)
        .subscribe((data) => {
          resolve(data);
        }, (error) => {
          reject(error);
        });
    });
  }

  getComic(id: number) {
    return new Promise((resolve, reject) => {
      this.api.get('v1/public/comics/' + id)
        .subscribe((data) => {
          resolve(data);
        }, (error) => {
          reject(error);
        });
    });
  }
}
