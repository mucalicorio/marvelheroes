import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';

@Injectable({
  providedIn: 'root'
})
export class CharactersService {

  constructor(private api: ApiService) { }

  getCharacters(limit: number = 10, offset: number) {
    return new Promise((resolve, reject) => {
      this.api.get('v1/public/characters', limit, offset)
        .subscribe((data) => {
          resolve(data);
        }, (error) => {
          reject(error);
        });
    });
  }

  getCharacter(id: number) {
    return new Promise((resolve, reject) => {
      this.api.get('v1/public/characters/' + id)
        .subscribe((data) => {
          resolve(data);
        }, (error) => {
          reject(error);
        });
    });
  }
}
