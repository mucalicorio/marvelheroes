import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  API_URL = 'https://gateway.marvel.com';
  PUBLIC_KEY_API = '5366b20f42a2e1c48f5b9002e44048f7';

  headers = new HttpHeaders()
    .set('Content-Type', 'application/json; charset=utf-8')
    .set('Accept', 'application/json');

  constructor(private httpClient: HttpClient) { }

  get(endpoint: string, limit: number = 20, offset: number = 20) {
    return this.httpClient.get(
      this.API_URL + '/' + endpoint + '?apikey=' + this.PUBLIC_KEY_API + '&limit=' + limit + '&offset=' + offset, { headers: this.headers }
    );
  }
}
